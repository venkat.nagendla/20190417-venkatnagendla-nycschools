//
//  Constants.swift
//  20190417-VenkatNagendla-NYCSchools
//
//  Created by Nagendla, Venkataha on 4/17/19.
//  Copyright © 2019 Nagendla, Venkataha. All rights reserved.
//


import UIKit

class SchoolsSatDetailsViewController: UIViewController {

    var arrayObject = ArrayObject()
    public var schoolSatObject : AnyObject = {} as AnyObject
    public var schoolObject    : AnyObject = {} as AnyObject
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //print("the school item is \(self.schoolObject) and sat object is \(self.schoolSatObject)")

        self.view.addSubview(schoolDetailsTableView);
        self.setupTableViewConstraints()
        self.registerCells()
    }
    
    func registerCells(){
        self.schoolDetailsTableView.register(SchoolDetailsTableViewCell.self, forCellReuseIdentifier:String(describing : SchoolDetailsTableViewCell.self))
    }
    lazy var schoolDetailsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = UIColor.lightGray
        tableView.accessibilityLabel = "ListTech Table"
        return tableView
    }()

    private func setupTableViewConstraints() {
        let barHeight=self.navigationController?.navigationBar.frame.height ?? 0

        NSLayoutConstraint.activate([
            schoolDetailsTableView.topAnchor.constraint(equalTo:view.topAnchor, constant: barHeight),
            schoolDetailsTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            schoolDetailsTableView.heightAnchor.constraint(equalToConstant:275),
            schoolDetailsTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            ])
    }
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SchoolsSatDetailsViewController:UITableViewDelegate,UITableViewDataSource {
    
    //MArk -UITableViewDelegateMethods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.schoolDetailsTableView.dequeueReusableCell(withIdentifier:"SchoolDetailsTableViewCell") as! SchoolDetailsTableViewCell
        cell.title_Label.attributedText = AppUtility.increaseTheSpacing(forTheText: self.arrayObject.schoolDetails[indexPath.row] as? String)

        switch indexPath.row {
        case 0:
            cell.sub_Label.attributedText = AppUtility.increaseTheSpacing(forTheText: self.schoolSatObject[GlobalVariables.satTestTakers] as? String)
        case 1:
            cell.sub_Label.attributedText = AppUtility.increaseTheSpacing(forTheText: self.schoolSatObject[GlobalVariables.readingAvgScore] as? String)
        case 2:
            cell.sub_Label.attributedText = AppUtility.increaseTheSpacing(forTheText: self.schoolSatObject[GlobalVariables.MathAvgScore] as? String)
        case 3:
            cell.sub_Label.attributedText = AppUtility.increaseTheSpacing(forTheText: self.schoolSatObject[GlobalVariables.WritingAvgScore] as? String)
        default:
            cell.sub_Label.text = ""
        }
        
        return cell
       
        
    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Constants.cellHeight.standardCellHeight.rawValue)
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        
      return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        let headerLbl = UILabel()
        headerLbl.font = UIFont.mainFontWithSFMedium()
        headerLbl.attributedText = AppUtility.increaseTheSpacing(forTheText: self.schoolObject[GlobalVariables.schoolName] as? String)
        headerLbl.textAlignment = NSTextAlignment.center
        headerLbl.textColor = UIColor.black
        headerLbl.frame = CGRect(x: 10, y: 0, width: tableView.frame.size.width-10, height: 50)
        headerLbl.textAlignment = .left
        headerView.addSubview(headerLbl)
        
        let separator = UILabel()
        separator.font = UIFont.mainFontWithSFBold()
        separator.backgroundColor = UIColor.lightGray
        separator.frame = CGRect(x: 10, y: 49, width: tableView.frame.size.width - 10, height: 1)
        separator.textAlignment = .left
        headerView.addSubview(separator)
        
        return headerView
    }
}
