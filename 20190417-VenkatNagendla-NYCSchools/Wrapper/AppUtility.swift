//
//  AppUtility.swift
//  20190417-VenkatNagendla-NYCSchools
//
//  Created by Nagendla, Venkataha on 4/17/19.
//  Copyright © 2019 Nagendla, Venkataha. All rights reserved.
//

import UIKit

class AppUtility: NSObject {
    
    static let sharedInstance = AppUtility()
    
    class func increaseTheSpacing(forTheText text:String?) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: text ?? "", attributes:[NSAttributedString.Key.kern: 0.6])
        
        return attributedString
    }
}
