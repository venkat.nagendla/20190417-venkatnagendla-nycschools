//
//  Constants.swift
//  20190417-VenkatNagendla-NYCSchools
//
//  Created by Nagendla, Venkataha on 4/17/19.
//  Copyright © 2019 Nagendla, Venkataha. All rights reserved.
//

import UIKit

enum Constants {

    enum fontSizes:Float {
        case HEADER_FONT = 16.0
        case MAIN_FONT   = 14.0
        case SUB_FONT    = 12.0
    }
    
    enum cellHeight:Float {
        case standardCellHeight = 45.0
    }
}
