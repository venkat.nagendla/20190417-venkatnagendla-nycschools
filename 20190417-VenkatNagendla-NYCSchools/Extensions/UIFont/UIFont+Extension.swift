//
//  AppDelegate.swift
//  20190417-VenkatNagendla-NYCSchools
//
//  Created by Nagendla, Venkataha on 4/17/19.
//  Copyright © 2019 Nagendla, Venkataha. All rights reserved.
//

import UIKit

extension UIFont {
    
   class func mainFontWithSFMedium() -> UIFont? {
        let mainFont = UIFont(name: NSLocalizedString("fontName", comment: ""), size: CGFloat(Constants.fontSizes.MAIN_FONT.rawValue))
       
        return mainFont
    }
    
   class func mainFontWithSFBold() -> UIFont? {
        let mainFont = UIFont(name: NSLocalizedString("fontName_BOLD", comment: ""), size: CGFloat(Constants.fontSizes.MAIN_FONT.rawValue))
        
        return mainFont
    }
    
    func subFontWithSFMedium() -> UIFont? {
        let mainFont = UIFont(name: NSLocalizedString("fontName", comment: ""), size: CGFloat(Constants.fontSizes.SUB_FONT.rawValue))
        
        return mainFont
    }

}
