//
//  Constants.swift
//  20190417-VenkatNagendla-NYCSchools
//
//  Created by Nagendla, Venkataha on 4/17/19.
//  Copyright © 2019 Nagendla, Venkataha. All rights reserved.
//


import UIKit
import Foundation

class WebService: NSObject {
    
    static let shared = WebService();
    var schoolsListArray      = Array<Any>()
    var schoolsSATscoresArray  = Array<Any>()
    private override init() {
        
    }
    
    public func downloadSatScores(){

        let url         = URL(string: GlobalVariables.schoolsSatListUrl)!
        let urlRequest  = URLRequest(url: url )
        let dataTask    = URLSession.shared.dataTask(with: urlRequest) { (Data, URLResponse, Error) in
            if(!(Error != nil)){
                do{
                    self.schoolsSATscoresArray = try JSONSerialization.jsonObject(with: Data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Array
                    //print("the schools sat array is \(self.schoolsSATscoresArray) count is \(String(describing: (self.schoolsSATscoresArray as AnyObject).count))")
                    
                }catch{
                    //print("got error")
                }
            }
        }
        
        dataTask.resume()
    }
    
    

}

struct GlobalVariables {
    static var schoolsListUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static var schoolsSatListUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    static var schoolsListCellId    = "SchoolsListCell"
    static var schoolName           = "school_name"
    static var schoolNmber          = "phone_number"
    static var schoolsCity          = "city"
    static var schoolsEmail         = "school_email"
    static var schoolsWebsite       = "website"
    
    static var dbn                  = "dbn"
    static var satTestTakers        = "num_of_sat_test_takers"
    static var readingAvgScore      = "sat_critical_reading_avg_score"
    static var MathAvgScore         = "sat_math_avg_score"
    static var WritingAvgScore      = "sat_writing_avg_score"
    static var satDetailsViewId     = "SchoolsSatDetailsViewController"
}

